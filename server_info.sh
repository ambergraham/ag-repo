#!/bin/bash

#Get the current date
current_date=$(date "+%Y-%m-%d")

# Display the current date
echo "Current Date: $current_date"

#Last 10 users who logged to a server
last_users=$(last | awk '{print $1}' | grep -v "wtmp" | grep -v "^$" | sort -u | head -n 10)

#Display the last 10 users
echo "Last 10 users logged in:"
echo "$last_users"

#swap space
swap_info=$(free -h | awk '/Swap:/ {print $2, $3, $4}')

# Display the swap space information
echo "Swap Space Information:"
echo "Total Used Free"
echo "$swap_info"

#kernel version
kernel_version=$(uname -r)

# Display the kernel version
echo "Kernel Version: $kernel_version"

#ip address
ip_address=$(ip route get 1 | awk '{print $NF; exit}')

# Display the IP address
echo "IP Address: $ip_address"
